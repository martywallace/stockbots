export default () => {
  return {
    dev: process.env.NODE_ENV !== 'production',
    db: {
      host: process.env.DB_HOST,
      port: process.env.DB_PORT,
      name: process.env.DB_NAME,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      cacert: process.env.DB_CACERT,
    },
  };
};
