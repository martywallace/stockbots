import { BadRequestException, Injectable } from '@nestjs/common';
import { BotEntity } from '../data/entities/bot';
import { TeamEntity } from '../data/entities/team';
import { UserEntity } from '../data/entities/user';
import { BotRepository } from '../data/repositories/bot';
import { TeamRepository } from '../data/repositories/team';
import { CreateTeamRequest } from '../http';

@Injectable()
export class TeamService {
  static readonly MAXIMUM_TOTAL_HEALTH = 1000;

  constructor(
    private readonly teamRepository: TeamRepository,
    private readonly botRepository: BotRepository,
  ) {}

  public async create(
    owner: UserEntity,
    model: CreateTeamRequest,
  ): Promise<TeamEntity> {
    const bots: readonly BotEntity[] = await this.botRepository
      .createQueryBuilder('bot')
      .where('bot.id IN (:...ids)')
      .andWhere('bot.userId = :userId')
      .setParameters({
        ids: model.bots,
        userId: owner.id,
      })
      .getMany();

    if (bots.length === 0) {
      throw new BadRequestException('Teams must contain at least one bot.');
    }

    const totalHealth: number = bots.reduce(
      (total, bot) => total + bot.health,
      0,
    );

    if (totalHealth > TeamService.MAXIMUM_TOTAL_HEALTH) {
      throw new BadRequestException('Total team health must not exceed 1,000.');
    }

    // todo: don't allow the same company multiple times.

    return await this.teamRepository.save({
      name: model.name,
      userId: owner.id,
      bots,
    });
  }
}
