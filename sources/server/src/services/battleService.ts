import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { BattleEntity } from '../data/entities/battle';
import { TeamEntity } from '../data/entities/team';
import { UserEntity } from '../data/entities/user';
import { BattleRepository } from '../data/repositories/battle';
import { TeamRepository } from '../data/repositories/team';
import { Simulation } from '../simulation';
import { StartBattleRequest } from '../http';

@Injectable()
export class BattleService {
  public readonly simulations: Simulation[] = [];

  constructor(
    private readonly battleRepository: BattleRepository,
    private readonly teamRepository: TeamRepository,
  ) {
    // On startup, resume any pending battles.
    // todo
  }

  public async startBattle(
    initiator: UserEntity,
    model: StartBattleRequest,
  ): Promise<BattleEntity> {
    if (model.teamId === model.targetTeamId) {
      throw new BadRequestException('Teams cannot fight themselves.');
    }

    const team: TeamEntity = await this.teamRepository.findOneOrFail(
      model.teamId,
    );
    const targetTeam: TeamEntity = await this.teamRepository.findOneOrFail(
      model.targetTeamId,
    );

    if (team.userId !== initiator.id) {
      throw new UnauthorizedException(
        'Local team does not belong to the current user.',
      );
    }

    const { id } = await this.battleRepository.save({
      initiator,
      teams: [team, targetTeam],
    });

    const battle: BattleEntity = await this.battleRepository.findSimulationReady(
      id,
    );

    this.simulations.push(new Simulation(battle));

    return battle;
  }
}
