import { BadRequestException, Injectable } from '@nestjs/common';
import { linear } from 'everpolate';
import { BotEntity } from '../data/entities/bot';
import { UserEntity } from '../data/entities/user';
import { BotRepository } from '../data/repositories/bot';
import { CreateBotRequest } from '../http';
import { SimplyWallStService } from './simplyWallStService';

@Injectable()
export class BotService {
  static readonly HP_MINIMUM = 50;
  static readonly HP_MAXIMUM = 1000;

  static readonly MARKET_CAP_5TH_PERCENTILE = 2142910;
  static readonly MARKET_CAP_50TH_PERCENTILE = 285471599;
  static readonly MARKET_CAP_95TH_PERCENTILE = 32831836643;
  static readonly MARKET_CAP_99TH_PERCENTILE = 142796425026;

  constructor(
    private readonly botRepository: BotRepository,
    private readonly simplyWallStService: SimplyWallStService,
  ) {}

  public async create(
    owner: UserEntity,
    model: CreateBotRequest,
  ): Promise<BotEntity> {
    const data = await this.simplyWallStService.getCompanyData(
      model.companySymbol,
    );

    if (!data.primary_ticker) {
      throw new BadRequestException('Only primary listings are permitted.');
    }

    return await this.botRepository.save({
      userId: owner.id,
      companyId: data.company_id,
      companyName: data.name,
      companySymbol: data.unique_symbol,
      companyCanonicalUrl: data.canonical_url,
      health: this.healthFromMarketCap(
        data.analysis.data.extended.data.analysis.value.market_cap_usd,
      ),
    });
  }

  public healthFromMarketCap(marketCap: number): number {
    const base: number = linear(
      marketCap,
      [
        BotService.MARKET_CAP_5TH_PERCENTILE,
        BotService.MARKET_CAP_50TH_PERCENTILE,
        BotService.MARKET_CAP_95TH_PERCENTILE,
        BotService.MARKET_CAP_99TH_PERCENTILE,
      ],
      [
        BotService.HP_MINIMUM,
        BotService.HP_MAXIMUM / 3,
        BotService.HP_MAXIMUM - BotService.HP_MAXIMUM / 6,
        BotService.HP_MAXIMUM,
      ],
    );

    return Math.round(
      Math.min(Math.max(base, BotService.HP_MINIMUM), BotService.HP_MAXIMUM),
    );
  }
}
