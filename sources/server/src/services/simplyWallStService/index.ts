import { Injectable } from '@nestjs/common';
import axios, { AxiosInstance, AxiosResponse } from 'axios';
import { SimplyWallStCompanyData } from './types';

@Injectable()
export class SimplyWallStService {
  private readonly client: AxiosInstance;

  constructor() {
    this.client = axios.create({
      baseURL: 'https://api.simplywall.st',
    });
  }

  public async getCompanyData(
    symbol: string,
  ): Promise<SimplyWallStCompanyData> {
    const response: AxiosResponse<{
      data: SimplyWallStCompanyData;
    }> = await this.client.get(`api/company/${symbol}`, {
      params: {
        include: 'info,score.snowflake,analysis.extended.value',
        version: '2.0',
      },
    });
    return response.data.data;
  }
}
