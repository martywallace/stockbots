export type SimplyWallStCompanyData = {
  readonly company_id: string;
  readonly name: string;
  readonly unique_symbol: string;
  readonly canonical_url: string;
  readonly primary_ticker: boolean;
  readonly analysis: {
    readonly data: {
      readonly extended: {
        readonly data: {
          readonly analysis: {
            readonly value: {
              readonly market_cap_usd: number;
              readonly last_share_price: number;
            };
          };
        };
      };
    };
  };
};
