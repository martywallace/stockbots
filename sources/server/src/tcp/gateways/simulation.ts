import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { JoinSimulationMessage, LeaveSimulationMessage } from '..';
import { BattleService } from '../../services/battleService';

@WebSocketGateway()
export class SimulationGateway
  implements
    OnGatewayConnection<Socket>,
    OnGatewayDisconnect<Socket>,
    OnGatewayInit<Server> {
  @WebSocketServer()
  public readonly server!: Server;

  constructor(private readonly battleService: BattleService) {}

  public afterInit(server: Server) {
    setInterval(() => {
      for (const simulation of this.battleService.simulations) {
        // Notify room of updates in running simulation.
        server
          .in(this.getRoomName(simulation.battle.id))
          .emit('update', simulation.getData());
      }
    }, 34);
  }

  public handleConnection(client: Socket): void {}
  public handleDisconnect(client: Socket): void {}

  @SubscribeMessage('join')
  public joinSimulation(
    @MessageBody() message: JoinSimulationMessage,
    @ConnectedSocket() socket: Socket,
  ): void {
    // We only care about one simulation at a time.
    socket.leaveAll();

    // Join it.
    socket.join(this.getRoomName(message.battleId));
  }

  @SubscribeMessage('leave')
  public leaveSimulation(
    @MessageBody() message: LeaveSimulationMessage,
    @ConnectedSocket() socket: Socket,
  ): void {
    socket.leave(this.getRoomName(message.battleId));
  }

  private getRoomName(battleId: number): string {
    return `simulation/${battleId}`;
  }
}
