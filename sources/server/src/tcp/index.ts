export type JoinSimulationMessage = {
  readonly battleId: number;
};
export type LeaveSimulationMessage = {
  readonly battleId: number;
};
