import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { TeamEntity } from './team';
import { UserEntity } from './user';

@Entity({
  name: 'bots',
})
export class BotEntity {
  @PrimaryGeneratedColumn()
  public readonly id!: number;

  @CreateDateColumn()
  @Index()
  public readonly created!: Date;

  @Column('uuid', { name: 'company_id' })
  public readonly companyId!: string;

  @Column('varchar', { name: 'company_symbol', length: 32 })
  public readonly companySymbol!: string;

  @Column('varchar', { name: 'company_name', length: 128 })
  public readonly companyName!: string;

  @Column('varchar', { name: 'company_canonical_url' })
  public readonly companyCanonicalUrl!: string;

  @Column('int', { name: 'user_id' })
  public readonly userId!: number;

  @ManyToOne(() => UserEntity, (user) => user.bots)
  @JoinColumn({ name: 'user_id' })
  public readonly user!: UserEntity;

  @ManyToMany(() => TeamEntity, (team) => team.bots)
  public readonly teams!: readonly TeamEntity[];

  @Column('int')
  public readonly health!: number;
}
