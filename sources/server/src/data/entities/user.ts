import {
  Column,
  CreateDateColumn,
  Entity,
  Generated,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BotEntity } from './bot';
import { TeamEntity } from './team';

@Entity({
  name: 'users',
})
export class UserEntity {
  @PrimaryGeneratedColumn()
  public readonly id!: number;

  @Generated('uuid')
  @Column('uuid', { unique: true })
  public readonly token!: string;

  @CreateDateColumn()
  @Index()
  public readonly created!: Date;

  @Column('inet', { select: false })
  public readonly ip!: string;

  @Column('varchar', { length: 128, nullable: true })
  public readonly name!: string | null;

  @OneToMany(() => TeamEntity, (team) => team.user, { cascade: true })
  public readonly teams!: readonly TeamEntity[];

  @OneToMany(() => BotEntity, (bot) => bot.user, { cascade: true })
  public readonly bots!: readonly BotEntity[];
}
