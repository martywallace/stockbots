import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { TeamEntity } from './team';
import { UserEntity } from './user';

@Entity({
  name: 'battles',
})
export class BattleEntity {
  @PrimaryGeneratedColumn()
  public readonly id!: number;

  @CreateDateColumn()
  public readonly started!: Date;

  @Column('timestamp', { nullable: true })
  public readonly ended!: Date | null;

  @Column('int', { name: 'initiator_id' })
  public readonly initiatorId!: number;

  @ManyToOne(() => UserEntity)
  @JoinColumn({ name: 'initiator_id' })
  public readonly initiator!: UserEntity;

  @ManyToMany(() => TeamEntity, (team) => team.battles)
  @JoinTable({
    name: 'battles_to_teams',
    joinColumn: { name: 'battle_id' },
    inverseJoinColumn: { name: 'team_id' },
  })
  public readonly teams!: readonly TeamEntity[];

  @Column('int', { name: 'winning_team_id', nullable: true })
  public readonly winningTeamId!: number | null;
}
