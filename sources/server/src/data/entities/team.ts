import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BattleEntity } from './battle';
import { BotEntity } from './bot';
import { UserEntity } from './user';

@Entity({
  name: 'teams',
})
export class TeamEntity {
  @PrimaryGeneratedColumn()
  public readonly id!: number;

  @CreateDateColumn()
  @Index()
  public readonly created!: Date;

  @Column('uuid', { name: 'user_id' })
  public readonly userId!: number;

  @ManyToOne(() => UserEntity, (user) => user.teams)
  @JoinColumn({ name: 'user_id' })
  public readonly user!: UserEntity;

  @Column('varchar', { length: 128 })
  public readonly name!: string;

  @ManyToMany(() => BotEntity, (bot) => bot.teams)
  @JoinTable({
    name: 'teams_to_bots',
    joinColumn: { name: 'team_id' },
    inverseJoinColumn: { name: 'bot_id' },
  })
  public readonly bots!: readonly BotEntity[];

  @ManyToMany(() => BattleEntity, (battle) => battle.teams)
  public readonly battles!: readonly BattleEntity[];
}
