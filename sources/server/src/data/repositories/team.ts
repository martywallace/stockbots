import { EntityRepository, Repository, SelectQueryBuilder } from 'typeorm';
import { GetTeamsRequest } from '../../http';
import { TeamEntity } from '../entities/team';

@EntityRepository(TeamEntity)
export class TeamRepository extends Repository<TeamEntity> {
  public async search(
    request: GetTeamsRequest,
  ): Promise<readonly TeamEntity[]> {
    return await this.buildSearchQuery(request)
      .skip(request.offset)
      .take(request.limit)
      .orderBy('team.created', 'DESC')
      .getMany();
  }

  public async countSearchResults(request: GetTeamsRequest): Promise<number> {
    return await this.buildSearchQuery(request).getCount();
  }

  private buildSearchQuery(
    request: GetTeamsRequest,
  ): SelectQueryBuilder<TeamEntity> {
    const builder: SelectQueryBuilder<TeamEntity> = this.createQueryBuilder(
      'team',
    );

    return builder;
  }
}
