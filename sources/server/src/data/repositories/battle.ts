import { EntityRepository, Repository } from 'typeorm';
import { BattleEntity } from '../entities/battle';

@EntityRepository(BattleEntity)
export class BattleRepository extends Repository<BattleEntity> {
  /**
   * Retrieve a battle with all the data required for a simulation to run.
   *
   * @param id The battle ID.
   */
  public async findSimulationReady(id: number): Promise<BattleEntity> {
    return await this.createQueryBuilder('battle')
      .leftJoinAndSelect('battle.teams', 'team')
      .leftJoinAndSelect('team.bots', 'bot')
      .where('battle.id = :id')
      .setParameter('id', id)
      .getOneOrFail();
  }
}
