import { EntityRepository, Repository } from 'typeorm';
import { BotEntity } from '../entities/bot';

@EntityRepository(BotEntity)
export class BotRepository extends Repository<BotEntity> {}
