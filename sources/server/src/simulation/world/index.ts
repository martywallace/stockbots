import { WorldData, WorldObjectData } from '../types';

export enum WorldObjectType {
  Bot = 'bot',
  Bullet = 'bullet',
}

export class WorldPosition {
  constructor(public x: number, public y: number) {}
}

export abstract class WorldObject {
  public readonly type!: WorldObjectType;
  public readonly position!: WorldPosition;
  public rotation: number = 0;

  constructor(public readonly id: number) {
    this.position = new WorldPosition(0, 0);
  }

  public abstract update(world: World): void;
  public abstract destroy(): void;

  public getData(): WorldObjectData {
    return {
      i: this.id,
      x: this.position.x,
      y: this.position.y,
      r: this.rotation,
      d: {},
    };
  }
}

export class World {
  private nextId: number = 1;

  private readonly objects: WorldObject[] = [];

  constructor() {}

  public update(): void {
    for (const object of this.objects) {
      object.update(this);
    }
  }

  public add<T extends WorldObject>(object: T): T {
    this.objects.push(object);
    return object;
  }

  public remove<T extends WorldObject>(object: T): T | null {
    const index: number = this.objects.indexOf(object);

    if (index >= 0) {
      this.objects.splice(index, 1);
      object.destroy();

      return object;
    }

    return null;
  }

  public destroy(): void {
    for (const object of this.objects) {
      object.destroy();
    }
  }

  public getData(): WorldData {
    return {
      objects: this.objects.map((object) => object.getData()),
    };
  }

  public getNextId(): number {
    return this.nextId++;
  }
}
