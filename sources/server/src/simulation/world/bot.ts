import { WorldObject, WorldObjectType } from '.';
import { BotEntity } from '../../data/entities/bot';
import { WorldObjectData } from '../types';

export type BotData = {
  readonly health: number;
  readonly maxHealth: number;
};

export class Bot extends WorldObject {
  public readonly type = WorldObjectType.Bot;

  private health: number;

  constructor(id: number, private readonly entity: BotEntity) {
    super(id);

    // Start with full HP obviously.
    this.health = entity.health;
  }

  public update(): void {
    this.position.x++;
  }

  public destroy(): void {
    //
  }

  public toJSON(): WorldObjectData<BotData> {
    return {
      ...super.getData(),
      d: {
        health: this.health,
        maxHealth: this.entity.health,
      },
    };
  }
}
