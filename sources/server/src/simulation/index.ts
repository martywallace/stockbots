import { BattleEntity } from '../data/entities/battle';
import { SimulationData } from './types';
import { World } from './world';
import { Bot } from './world/bot';

export class Simulation {
  private readonly world: World;
  private readonly updateInterval: NodeJS.Timer;

  constructor(public readonly battle: BattleEntity) {
    this.world = new World();

    this.startup();

    this.updateInterval = setInterval(() => this.update(), 34);
  }

  private startup(): void {
    for (const team of this.battle.teams) {
      console.log(`Placing team ${team.name}.`);

      for (const bot of team.bots) {
        this.world.add(new Bot(this.world.getNextId(), bot));
      }
    }
  }

  private update(): void {
    this.world.update();
  }

  public destroy(): void {
    clearInterval(this.updateInterval);

    this.world.destroy();
  }

  public getData(): SimulationData {
    return {
      world: this.world.getData(),
    };
  }
}
