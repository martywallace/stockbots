import { UserEntity } from './data/entities/user';

declare global {
  namespace Express {
    export interface User extends UserEntity {}
  }
}
