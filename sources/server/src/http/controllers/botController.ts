import { Body, Controller, Post, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { CreateBotRequest } from '..';
import { BotEntity } from '../../data/entities/bot';
import { BotRepository } from '../../data/repositories/bot';
import { BotService } from '../../services/botService';
import { JoiPipe } from '../pipes/joi';

@Controller('bots')
export class BotController {
  constructor(
    private readonly botRepository: BotRepository,
    private readonly botService: BotService,
  ) {}

  @Post()
  @UseGuards(AuthGuard())
  public async create(
    @Req() request: Request,
    @Body(new JoiPipe(CreateBotRequest.getSchema())) body: CreateBotRequest,
  ): Promise<BotEntity> {
    return await this.botService.create(request.user!, body);
  }
}
