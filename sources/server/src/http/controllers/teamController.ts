import {
  Body,
  Controller,
  Get,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { CreateTeamRequest, GetTeamsRequest, GetTeamsResponse } from '..';
import { TeamEntity } from '../../data/entities/team';
import { TeamRepository } from '../../data/repositories/team';
import { TeamService } from '../../services/teamService';
import { JoiPipe } from '../pipes/joi';

@Controller('teams')
export class TeamController {
  constructor(
    private readonly teamRepository: TeamRepository,
    private readonly teamService: TeamService,
  ) {}

  @Get()
  public async search(
    @Query(new JoiPipe(GetTeamsRequest.getSchema())) query: GetTeamsRequest,
  ): Promise<GetTeamsResponse> {
    return {
      offset: query.offset,
      limit: query.limit,
      total: await this.teamRepository.countSearchResults(query),
      data: await this.teamRepository.search(query),
    };
  }

  @Post()
  @UseGuards(AuthGuard())
  public async create(
    @Req() request: Request,
    @Body(new JoiPipe(CreateTeamRequest.getSchema()))
    body: CreateTeamRequest,
  ): Promise<TeamEntity> {
    return await this.teamService.create(request.user!, body);
  }
}
