import { Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { UserEntity } from '../../data/entities/user';
import { UserRepository } from '../../data/repositories/user';

@Controller('users')
export class UserController {
  constructor(private readonly userRepository: UserRepository) {}

  @Get()
  @UseGuards(AuthGuard())
  public async me(@Req() request: Request): Promise<UserEntity> {
    return request.user!;
  }

  @Post()
  public async create(@Req() request: Request): Promise<UserEntity> {
    return await this.userRepository.save({
      ip: request.ip,
    });
  }
}
