import { Body, Controller, Post, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { StartBattleRequest } from '..';
import { BattleEntity } from '../../data/entities/battle';
import { BattleService } from '../../services/battleService';
import { JoiPipe } from '../pipes/joi';

@Controller('battles')
export class BattleController {
  constructor(private readonly battleService: BattleService) {}

  @Post()
  @UseGuards(AuthGuard())
  public async start(
    @Req() request: Request,
    @Body(new JoiPipe(StartBattleRequest.getSchema())) body: StartBattleRequest,
  ): Promise<BattleEntity> {
    return await this.battleService.startBattle(request.user!, body);
  }
}
