import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-http-bearer';
import { UserEntity } from '../../data/entities/user';
import { UserRepository } from '../../data/repositories/user';

@Injectable()
export class BearerTokenStrategy extends PassportStrategy(Strategy) {
  constructor(protected readonly userRepository: UserRepository) {
    super();
  }

  public async validate(token: string): Promise<UserEntity> {
    try {
      return await this.userRepository.findOneOrFail({
        token,
      });
    } catch (error) {
      throw new UnauthorizedException();
    }
  }
}
