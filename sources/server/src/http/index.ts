import { array, number, object, ObjectSchema, string } from '@hapi/joi';
import { TeamEntity } from '../data/entities/team';

export class GetTeamsRequest {
  public static getSchema(): ObjectSchema {
    return object().keys({
      offset: number().default(0).min(0),
      limit: number().default(50).max(250),
    });
  }

  public readonly offset!: number;
  public readonly limit!: number;
}

export type GetTeamsResponse = {
  readonly data: readonly TeamEntity[];
  readonly offset: number;
  readonly limit: number;
  readonly total: number;
};

export class CreateTeamRequest {
  public static getSchema(): ObjectSchema {
    return object().keys({
      name: string().required(),
      bots: array().items(number()).required().min(1).max(25),
    });
  }

  public readonly name!: string;
  public readonly bots!: readonly number[];
}

export class CreateBotRequest {
  public static getSchema(): ObjectSchema {
    return object().keys({
      companySymbol: string().required(),
    });
  }

  public readonly companySymbol!: string;
}

export class StartBattleRequest {
  public static getSchema(): ObjectSchema {
    return object().keys({
      teamId: number().required(),
      targetTeamId: number().required(),
    });
  }

  public readonly teamId!: number;
  public readonly targetTeamId!: number;
}
