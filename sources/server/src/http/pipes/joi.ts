import {
  ObjectSchema,
  ValidationError,
  ValidationErrorItem,
  ValidationResult,
} from '@hapi/joi';
import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';

@Injectable()
export class JoiPipe implements PipeTransform {
  constructor(protected readonly schema: ObjectSchema) {}

  public transform<T>(value: T, metadata: ArgumentMetadata): T {
    const result: ValidationResult = this.schema.validate(value, {
      abortEarly: false,
    });

    if (result.error) {
      throw new BadRequestException({
        statusCode: 400,
        message: 'Request failed validation.',
        details: this.formatError(result.error),
      });
    }

    return result.value;
  }

  protected formatError(errors: ValidationError): readonly unknown[] {
    return errors.details.map((item: ValidationErrorItem) => ({
      path: item.path.join('.'),
      message: item.message,
    }));
  }
}
