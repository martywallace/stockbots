import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { resolve as resolvePath } from 'path';
import config from './config';
import { BattleRepository } from './data/repositories/battle';
import { BotRepository } from './data/repositories/bot';
import { TeamRepository } from './data/repositories/team';
import { UserRepository } from './data/repositories/user';
import { SimulationGateway } from './tcp/gateways/simulation';
import { BattleController } from './http/controllers/battleController';
import { BotController } from './http/controllers/botController';
import { TeamController } from './http/controllers/teamController';
import { UserController } from './http/controllers/userController';
import { BearerTokenStrategy } from './http/strategies/bearerTokenStrategy';
import { BattleService } from './services/battleService';
import { BotService } from './services/botService';
import { SimplyWallStService } from './services/simplyWallStService';
import { TeamService } from './services/teamService';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [config],
    }),
    PassportModule.register({ defaultStrategy: 'bearer' }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (config: ConfigService) => ({
        type: 'postgres',
        host: config.get('db.host'),
        port: config.get('db.port'),
        database: config.get('db.name'),
        username: config.get('db.username'),
        password: config.get('db.password'),
        ssl: config.get('db.cacert')
          ? { ca: config.get('db.cacert') }
          : undefined,
        logging: 'all',
        entities: [
          resolvePath(__dirname, './data/{entities,repositories}/**/*.{js,ts}'),
        ],
        migrations: [resolvePath(__dirname, './data/migrations/*.{js,ts}')],
        migrationsRun: config.get('dev', false),
        synchronize: config.get('dev', false),
      }),
    }),
    TypeOrmModule.forFeature([
      UserRepository,
      TeamRepository,
      BotRepository,
      BattleRepository,
    ]),
  ],
  controllers: [
    UserController,
    TeamController,
    BotController,
    BattleController,
  ],
  providers: [
    BearerTokenStrategy,
    TeamService,
    BotService,
    SimplyWallStService,
    BattleService,
    SimulationGateway,
  ],
})
export class App {}

async function bootstrap() {
  const app = await NestFactory.create(App);
  await app.listen(3001);
}

bootstrap();
