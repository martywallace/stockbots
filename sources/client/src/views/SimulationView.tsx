import React, { FC } from "react";
import { useLocation, useParams } from "react-router-dom";
import SimulationRenderer from "../simulation/components/SimulationRenderer";

type SimulationViewParams = {
  readonly id: string;
};

const SimulationView: FC = () => {
  const { id } = useParams<SimulationViewParams>();

  return (
    <section>
      <SimulationRenderer battleId={parseInt(id)} />
    </section>
  );
};

export default SimulationView;
