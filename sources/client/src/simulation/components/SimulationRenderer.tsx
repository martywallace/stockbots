import React, { FC, useEffect, useRef } from "react";
import { useSocket } from "../../socket/useSocket";
import { SimulationScene } from "../rendering/scene";
import { SimulationData } from "../types";

type SimulationRendererProps = {
  readonly battleId: number;
};

const SimulationRenderer: FC<SimulationRendererProps> = ({ battleId }) => {
  const socket = useSocket();
  const canvas = useRef<HTMLCanvasElement>(null);

  useEffect(() => {
    const simulation: SimulationScene | null = canvas.current
      ? new SimulationScene(canvas.current)
      : null;

    // Connect to the server-side simulation.
    socket.emit("join", { battleId });

    // Start consuming simulation update data.
    socket.on("update", (data: SimulationData) => {
      if (simulation) {
        simulation.update(data);
      }
    });

    return () => {
      // Leave simulation when navigating away.
      socket.off("update");
      socket.emit("leave", { battleId });

      if (simulation) {
        simulation.destroy();
      }
    };
  });

  return (
    <section>
      <h2>Battle #{battleId}</h2>
      <div>
        <canvas ref={canvas}></canvas>
      </div>
    </section>
  );
};

export default SimulationRenderer;
