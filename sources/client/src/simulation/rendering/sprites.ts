import { Graphics } from "pixi.js";

export class CircleSprite extends Graphics {
  constructor() {
    super();

    this.beginFill(0xde3249, 1);
    this.drawCircle(0, 0, 50);
    this.endFill();
  }
}
