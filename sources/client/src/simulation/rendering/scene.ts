import { Application, Container, DisplayObject } from "pixi.js";
import { SimulationData, SimulationDataDiff } from "../types";
import { CircleSprite } from "./sprites";

type SimulationObject = {
  readonly graphics: DisplayObject;
};

export class SimulationScene {
  private readonly app: Application;
  private readonly stage: Container;

  private lastData: SimulationData | null = null;
  private objects: { [id: number]: SimulationObject } = {};

  constructor(canvas: HTMLCanvasElement) {
    this.app = new Application({
      view: canvas,
      resolution: window.devicePixelRatio || 1,
    });

    this.stage = new Container();
    this.app.stage.addChild(this.stage);
  }

  public update(data: SimulationData): void {
    const diff: SimulationDataDiff = this.lastData
      ? this.diff(data, this.lastData)
      : {
          world: {
            additions: data.world.objects.map((object) => object.i),
            deletions: [],
          },
        };

    // Add new objects.
    for (const id of diff.world.additions) {
      const object: SimulationObject = {
        graphics: new CircleSprite(),
      };

      this.stage.addChild(object.graphics);
      this.objects[id] = object;
    }

    // Remove deleted objects.
    for (const id of diff.world.deletions) {
      const object: SimulationObject | undefined = this.objects[id];

      if (object) {
        this.stage.removeChild(object.graphics);
        delete this.objects[id];
      }
    }

    // Update positions.
    for (const object of data.world.objects) {
      const simulationObject: SimulationObject | undefined = this.objects[
        object.i
      ];

      if (simulationObject) {
        simulationObject.graphics.x = object.x;
        simulationObject.graphics.y = object.y;
        simulationObject.graphics.rotation = object.r;
      }
    }

    this.lastData = data;
  }

  /**
   * Compute additions and deletions against the last simulation state.
   *
   * @param incoming The new simulation state.
   * @param previous The previous simulation state.
   */
  public diff(
    incoming: SimulationData,
    previous: SimulationData
  ): SimulationDataDiff {
    const incomingIds: readonly number[] = incoming.world.objects.map(
      (object) => object.i
    );
    const previousIds: readonly number[] = previous
      ? previous.world.objects.map((object) => object.i)
      : [];

    return {
      world: {
        additions: incomingIds.filter((id) => !previousIds.includes(id)),
        deletions: previousIds.filter((id) => !incomingIds.includes(id)),
      },
    };
  }

  public destroy(): void {
    this.app.destroy();
  }
}
