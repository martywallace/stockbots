export type WorldObjectData<T = {}> = {
  readonly i: number;
  readonly x: number;
  readonly y: number;
  readonly r: number;
  readonly d: T;
};

export type WorldData = {
  readonly objects: readonly WorldObjectData<any>[];
};

export type SimulationData = {
  readonly world: WorldData;
};

export type SimulationDataDiff = {
  readonly world: WorldDataDiff;
};

export type WorldDataDiff = {
  readonly additions: readonly number[];
  readonly deletions: readonly number[];
};
