import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { useSocket } from "./socket/useSocket";
import LandingView from "./views/LandingView";
import SimulationView from "./views/SimulationView";

const App = () => {
  // Connect socket immediately.
  const socket = useSocket();

  return (
    <main>
      <Router>
        <Switch>
          <Route path="/battle/:id" render={() => <SimulationView />} />
          <Route path="/" render={() => <LandingView />} />
        </Switch>
      </Router>
    </main>
  );
};

export default App;
