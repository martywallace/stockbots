import io from "socket.io-client";

const socketHost: string = "http://localhost:3001";

const socket: SocketIOClient.Socket = io(socketHost, {
  autoConnect: true,
});

export function useSocket(): SocketIOClient.Socket {
  return socket;
}
